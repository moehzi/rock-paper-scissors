'use strict';

var utils = require('../utils/writer.js');
var User = require('../service/UserService');

module.exports.v1UsersGET = function v1UsersGET (req, res, next) {
  User.v1UsersGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1UsersProfileGET = function v1UsersProfileGET (req, res, next) {
  User.v1UsersProfileGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1UsersProfilePUT = function v1UsersProfilePUT (req, res, next) {
  User.v1UsersProfilePUT()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1UsersStatsGET = function v1UsersStatsGET (req, res, next) {
  User.v1UsersStatsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
