'use strict';

var utils = require('../utils/writer.js');
var Room = require('../service/RoomService');

module.exports.createRoom = function createRoom (req, res, next, body) {
  Room.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.joinRoom = function joinRoom (req, res, next, body) {
  Room.joinRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsGET = function v1RoomsGET (req, res, next) {
  Room.v1RoomsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
