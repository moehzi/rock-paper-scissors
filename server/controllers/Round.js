'use strict';

var utils = require('../utils/writer.js');
var Round = require('../service/RoundService');

module.exports.v1RoundsGET = function v1RoundsGET (req, res, next) {
  Round.v1RoundsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoundsHistoryGET = function v1RoundsHistoryGET (req, res, next) {
  Round.v1RoundsHistoryGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
