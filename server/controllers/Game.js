'use strict';

var utils = require('../utils/writer.js');
var Game = require('../service/GameService');

module.exports.fight = function fight (req, res, next, body) {
  Game.fight(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1GamesHistoryGET = function v1GamesHistoryGET (req, res, next) {
  Game.v1GamesHistoryGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1GamesUserGET = function v1GamesUserGET (req, res, next, levelId) {
  Game.v1GamesUserGET(levelId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
