'use strict';


/**
 * Play the game
 * This endpoint to play the game
 *
 * body Object 
 * returns inline_response_201_3
 **/
exports.fight = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "status" : "PLAYER_2_TURN",
    "room" : {
      "id" : 1,
      "roundCount" : 1
    }
  },
  "message" : "Wait for player 2 to pick!",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get game history
 * This endpoint to get your game history
 *
 * returns inline_response_200_7
 **/
exports.v1GamesHistoryGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "user" : {
      "id" : 1,
      "username" : "moehzi",
      "updated_at" : "30-07-2021 12:00 +07:00",
      "created_at" : "30-07-2021 12:00 +07:00"
    },
    "room" : {
      "updated_at" : "30-07-2021 12:00 +07:00",
      "created_at" : "30-07-2021 12:00 +07:00",
      "id" : 1,
      "winnerId" : 1,
      "roundCount" : 3
    }
  }, {
    "id" : 2,
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "user" : {
      "id" : 1,
      "username" : "moehzi",
      "updated_at" : "30-07-2021 12:00 +07:00",
      "created_at" : "30-07-2021 12:00 +07:00"
    },
    "room" : {
      "updated_at" : "30-07-2021 12:00 +07:00",
      "created_at" : "30-07-2021 12:00 +07:00",
      "id" : 1,
      "roundCount" : 2
    }
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all user with poin and level
 * Use this endpoint to get all user with point and level and you can fight them.
 *
 * levelId BigDecimal  (optional)
 * returns inline_response_200_4
 **/
exports.v1GamesUserGET = function(levelId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "player" : {
      "username" : "moehzi",
      "level" : "NOVICE",
      "point" : 1000
    }
  }, {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 2,
    "player" : {
      "username" : "dinduy",
      "level" : "GRAND MASTER",
      "point" : 5000
    }
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

