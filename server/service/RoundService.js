'use strict';


/**
 * Get all history rounds
 * Use this endpoint to get all history rounds. Only admin can access this site
 *
 * returns inline_response_200_3
 **/
exports.v1RoundsGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "winner" : {
      "id" : 1,
      "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
      "bio" : "Saya lebih suka nasi padang daripada nasi kuning",
      "phoneNumber" : 6281354592288,
      "address" : "Jalan suka hati gunung bromo",
      "created_at" : "30-07-2021 12:00 +07:00",
      "updated_at" : "30-07-2021 12:00 +07:00"
    },
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "room" : {
      "updated_at" : "30-07-2021 12:00 +07:00",
      "winner" : {
        "id" : 1,
        "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
        "bio" : "Saya lebih suka nasi padang daripada nasi kuning",
        "phoneNumber" : 6281354592288,
        "address" : "Jalan suka hati gunung bromo",
        "created_at" : "30-07-2021 12:00 +07:00",
        "updated_at" : "30-07-2021 12:00 +07:00"
      },
      "created_at" : "30-07-2021 12:00 +07:00",
      "id" : 1,
      "name" : "Khusus orang sulawesi",
      "roundCount" : 3
    },
    "status" : "FINISHED"
  }, {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "winner" : {
      "id" : 2,
      "avatarUrl" : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYnQ1fiWAmZcN7o5bJSTgoLXGGj2unA5SUFqxuUAFFlnhS0177Amw_2eumiuKBq9k_oX0&usqp=CAU",
      "bio" : "The real men use three pedals",
      "phoneNumber" : 628135453124,
      "address" : "Jalan tritura Toko Baju",
      "created_at" : "30-07-2021 13:00 +07:00",
      "updated_at" : "30-07-2021 13:00 +07:00"
    },
    "created_at" : "30-07-2021 13:00 +07:00",
    "id" : 2,
    "name" : "Cari Pacar",
    "roundCount" : 1,
    "room" : {
      "updated_at" : "30-07-2021 12:00 +07:00",
      "winner" : {
        "id" : 1,
        "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
        "bio" : "Saya lebih suka nasi padang daripada nasi kuning",
        "phoneNumber" : 6281354592288,
        "address" : "Jalan suka hati gunung bromo",
        "created_at" : "30-07-2021 12:00 +07:00",
        "updated_at" : "30-07-2021 12:00 +07:00"
      },
      "created_at" : "30-07-2021 12:00 +07:00",
      "id" : 1,
      "name" : "Khusus orang sulawesi",
      "roundCount" : 3
    },
    "status" : "FINISHED"
  }, {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "winner" : {
      "id" : 2,
      "avatarUrl" : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYnQ1fiWAmZcN7o5bJSTgoLXGGj2unA5SUFqxuUAFFlnhS0177Amw_2eumiuKBq9k_oX0&usqp=CAU",
      "bio" : "The real men use three pedals",
      "phoneNumber" : 628135453124,
      "address" : "Jalan tritura Toko Baju",
      "created_at" : "30-07-2021 13:00 +07:00",
      "updated_at" : "30-07-2021 13:00 +07:00"
    },
    "created_at" : "30-07-2021 13:00 +07:00",
    "id" : 3,
    "name" : "Cari Pacar",
    "roundCount" : 1,
    "room" : {
      "updated_at" : "30-07-2021 12:00 +07:00",
      "winner" : {
        "id" : 1,
        "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
        "bio" : "Saya lebih suka nasi padang daripada nasi kuning",
        "phoneNumber" : 6281354592288,
        "address" : "Jalan suka hati gunung bromo",
        "created_at" : "30-07-2021 12:00 +07:00",
        "updated_at" : "30-07-2021 12:00 +07:00"
      },
      "created_at" : "30-07-2021 12:00 +07:00",
      "id" : 1,
      "name" : "Khusus orang sulawesi",
      "roundCount" : 3
    },
    "status" : "FINISHED"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get round history
 * This endpoint to get your round history
 *
 * returns inline_response_200_6
 **/
exports.v1RoundsHistoryGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "user" : {
      "id" : 1,
      "username" : "moehzi"
    },
    "round" : {
      "id" : 1,
      "status" : "PLAYER_1_TURN",
      "winnerId" : 1,
      "roomId" : 1
    },
    "gameOptionId" : 1
  }, {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "user" : {
      "id" : 1,
      "username" : "moehzi"
    },
    "round" : {
      "id" : 2,
      "status" : "PLAYER_2_TURN",
      "winnerId" : 1,
      "roomId" : 1
    },
    "gameOptionId" : 1
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

