'use strict';


/**
 * Get all users
 * Use this endpoint to get all the users. Only admin can access this site
 *
 * returns inline_response_200_1
 **/
exports.v1UsersGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
    "bio" : "Saya lebih suka nasi padang daripada nasi kuning",
    "phoneNumber" : 6281354592288,
    "address" : "Jalan suka hati gunung bromo",
    "created_at" : "30-07-2021 12:00 +07:00",
    "user" : {
      "updated_at" : "30-07-2021 12:00 +07:00",
      "created_at" : "30-07-2021 12:00 +07:00",
      "id" : 1,
      "username" : "moehzi"
    }
  }, {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "id" : 2,
    "avatarUrl" : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYnQ1fiWAmZcN7o5bJSTgoLXGGj2unA5SUFqxuUAFFlnhS0177Amw_2eumiuKBq9k_oX0&usqp=CAU",
    "bio" : "The real men use three pedals",
    "phoneNumber" : 628135453124,
    "address" : "Jalan tritura Toko Baju",
    "created_at" : "30-07-2021 13:00 +07:00",
    "user" : {
      "id" : 2,
      "username" : "dinduy",
      "updated_at" : "30-07-2021 13:00 +07:00",
      "created_at" : "30-07-2021 13:00 +07:00"
    }
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get my profile
 * Use this endpoint to get your profile.
 *
 * returns inline_response_200_8
 **/
exports.v1UsersProfileGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "user" : {
      "username" : "moehzi"
    },
    "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
    "bio" : "Memanggil kamu",
    "phoneNumber" : 6285359123,
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Edit my profile
 * Use this endpoint to edit your profile.
 *
 * returns inline_response_200_8
 **/
exports.v1UsersProfilePUT = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "user" : {
      "username" : "moehzi"
    },
    "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
    "bio" : "Memanggil kamu",
    "phoneNumber" : 6285359123,
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get my stats
 * Use this endpoint to get your stat.
 *
 * returns inline_response_200_5
 **/
exports.v1UsersStatsGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "userId" : 1,
    "level" : {
      "name" : "NOVICE"
    },
    "point" : 1000
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

