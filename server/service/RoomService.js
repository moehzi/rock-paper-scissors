'use strict';


/**
 * Create Room and fight enemy
 * This endpoint will automitcally create a new room after you fill the field to choose your enemy.
 *
 * body Object 
 * returns inline_response_201_1
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "name" : "Yok main",
    "roundCount" : 0
  },
  "message" : "Succesfully create room!",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Join room and start the fight!
 * This endpoint will generate round one and you can play now
 *
 * body Object 
 * returns inline_response_201_2
 **/
exports.joinRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "name" : "Yok main",
    "roundCount" : 1
  },
  "message" : "Succesfully join room you can play now!",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all history rooms
 * Use this endpoint to get all history rooms. Only admin can access this site
 *
 * returns inline_response_200_2
 **/
exports.v1RoomsGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "winner" : {
      "id" : 1,
      "avatarUrl" : "https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png",
      "bio" : "Saya lebih suka nasi padang daripada nasi kuning",
      "phoneNumber" : 6281354592288,
      "address" : "Jalan suka hati gunung bromo",
      "created_at" : "30-07-2021 12:00 +07:00",
      "updated_at" : "30-07-2021 12:00 +07:00"
    },
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "name" : "Khusus orang sulawesi",
    "roundCount" : 3
  }, {
    "updated_at" : "30-07-2021 12:00 +07:00",
    "winner" : {
      "id" : 2,
      "avatarUrl" : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYnQ1fiWAmZcN7o5bJSTgoLXGGj2unA5SUFqxuUAFFlnhS0177Amw_2eumiuKBq9k_oX0&usqp=CAU",
      "bio" : "The real men use three pedals",
      "phoneNumber" : 628135453124,
      "address" : "Jalan tritura Toko Baju",
      "created_at" : "30-07-2021 13:00 +07:00",
      "updated_at" : "30-07-2021 13:00 +07:00"
    },
    "created_at" : "30-07-2021 13:00 +07:00",
    "id" : 2,
    "name" : "Cari Pacar",
    "roundCount" : 1
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

