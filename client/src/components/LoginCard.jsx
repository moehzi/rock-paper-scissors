import { useState } from "react";
import { useNavigate } from "react-router-dom";
import userData from "../data/user.json";

const LoginCard = () => {
  let navigate = useNavigate();
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  const handleFormSubmission = (e) => {
    e.preventDefault();

    const userCorrect = userData.find((u) => {
      return u.username === username;
    });

    const passwordCorrect = userData.find((u) => {
      return u.password === password;
    });

    const user = userData.find((u) => {
      return u.username === username && u.password === password;
    });

    const indexOfUser = userData.indexOf(user);
    console.log(indexOfUser);

    if (!userCorrect) {
      setErrorMessage("Username is not registered!");
      return;
    }

    if (!passwordCorrect) {
      setErrorMessage("Password is not correct!");
      return;
    }
    if (user) {
      navigate("/dashboard");
      return;
    }
  };

  return (
    <div className="card">
      <h1 className="h1">LOGIN</h1>
      <form className="form" onSubmit={handleFormSubmission}>
        <input
          placeholder="Insert your username"
          className="form-input"
          name="username"
          onChange={(e) => setUsername(e.target.value)}
        ></input>
        <input
          placeholder="Insert your password"
          className="form-input"
          name="password"
          onChange={(e) => setPassword(e.target.value)}
        ></input>
        <button className="btn-primary">Login</button>
        {!!errorMessage && <p className="error">{errorMessage}</p>}
      </form>
    </div>
  );
};
export default LoginCard;
