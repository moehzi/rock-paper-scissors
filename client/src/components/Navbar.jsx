import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar-container">
      <nav className="navbar">
        <div className="logo">
          <h1>SUIT GAME</h1>
        </div>
        <ul className="list-wrapper">
          <Link to={"/login"}>
            <li className="list-item">Login</li>
          </Link>
          <Link to={"/register"}>
            <li className="list-item">Register</li>
          </Link>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
