const PlayerCard = (props) => {
  return (
    <div className="player-card">
      <div className="profile-description">
        <img src={props.avatar} alt="" className="img-profile" />
        <div>
          <h3 className="name">{props.name}</h3>
          <p className="level">{props.level}</p>
        </div>
      </div>
      <div className="profile-text">
        <p className="comment">{props.bio}</p>
      </div>
      <p className="date">Since {props.date}</p>
      <button className="btn-primary btn-fight">FIGHT</button>
    </div>
  );
};

export default PlayerCard;
