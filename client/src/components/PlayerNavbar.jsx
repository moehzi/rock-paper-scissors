const Navbar = (props) => {
  return (
    <div className="navbar-container">
      <nav className="navbar">
        <div className="logo">
          <h1>SUIT GAME</h1>
        </div>
        <ul className="list-wrapper">
          <h1>{props.name}</h1>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
