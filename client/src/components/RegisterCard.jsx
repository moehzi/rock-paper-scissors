import { useNavigate } from "react-router-dom";

const RegisterCard = () => {
  const navigate = useNavigate();
  const handleFormSubmit = (e) => {
    e.preventDefault();
    navigate("/login");
  };
  return (
    <div className="card">
      <h1 className="h1">REGISTER</h1>
      <form className="form" onSubmit={handleFormSubmit}>
        <input
          placeholder="Insert your username"
          className="form-input"
        ></input>
        <input
          placeholder="Insert your password"
          className="form-input"
        ></input>
        <input
          placeholder="Confirm your password"
          className="form-input"
        ></input>
        <button className="btn-primary">Register</button>
      </form>
    </div>
  );
};
export default RegisterCard;
