import "../assets/css/App.css";
import LoginCard from "../components/LoginCard";
const Login = () => {
  return (
    <div className="center h-100">
      <LoginCard />
    </div>
  );
};

export default Login;
