import "../assets/css/App.css";
import PlayerNavbar from "../components/PlayerNavbar";
import PlayerCard from "../components/PlayerCard";
import { useState } from "react";
import opponentsData from "../data/opponents.json";
import moment from "moment";

const HomeLogin = () => {
  const [playerLevel, setPlayerLevel] = useState("NOVICE");

  const filteredPlayer = opponentsData.filter((opponent) => {
    return opponent.level === playerLevel;
  });

  const playerList = filteredPlayer.map((e) => {
    return (
      <div key={e.id}>
        <PlayerCard
          avatar={e.avatar}
          level={e.level}
          bio={e.bio}
          name={e.name}
          date={moment(e.createdAt).format("MMMM DD, YYYY")}
        />
      </div>
    );
  });

  const defaultCard = opponentsData.map((e) => {
    return (
      <div key={e.id}>
        <PlayerCard
          avatar={e.avatar}
          level={e.level}
          bio={e.bio}
          name={e.name}
          date={moment(e.createdAt).format("MMMM DD, YYYY")}
        />
      </div>
    );
  });

  const isSelected = playerLevel.level !== "";

  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };
  return (
    <div className="h-100">
      <PlayerNavbar />
      <div className="player-list__container">
        <h1 className="h1 text-white h1__header">Choose your opponent</h1>
        <div className="filter-navigation">
          <div className="list-filter">
            <button
              data-value="NOVICE"
              className={
                toggleState === 1
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(1);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              NOVICE
            </button>
          </div>
          <div className="list-filter">
            <button
              data-value="CLASS A"
              className={
                toggleState === 2
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(2);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              CLASS A
            </button>
          </div>
          <div className="list-filter">
            <button
              data-value="CLASS B"
              className={
                toggleState === 3
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(3);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              CLASS B
            </button>
          </div>
          <div className="list-filter">
            <button
              data-value="CLASS C"
              className={
                toggleState === 4
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(4);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              CLASS C
            </button>
          </div>
          <div className="list-filter">
            <button
              data-value="CLASS D"
              className={
                toggleState === 5
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(5);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              CLASS D
            </button>
          </div>
          <div className="list-filter">
            <button
              data-value="CANDIDATE MASTER"
              className={
                toggleState === 6
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(6);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              CANDIDATE MASTER
            </button>
          </div>
          <div className="list-filter">
            <button
              data-value="GRAND MASTER"
              className={
                toggleState === 7
                  ? "list-filter__item active"
                  : "list-filter__item"
              }
              onClick={(e) => {
                toggleTab(7);
                setPlayerLevel(e.target.dataset.value);
              }}
            >
              GRAND MASTER
            </button>
          </div>
        </div>
        <div className="player-card__container">
          {isSelected ? playerList : defaultCard}
        </div>
      </div>
    </div>
  );
};

export default HomeLogin;
