import "../assets/css/App.css";
import RegisterCard from "../components/RegisterCard";

const Register = () => {
  return (
    <div className="center h-100">
      <RegisterCard />
    </div>
  );
};

export default Register;
